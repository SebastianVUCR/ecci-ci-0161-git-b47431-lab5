package cr.ac.ucr.ecci.eseg.miserviciossensores;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import android.os.Looper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class MainActivity extends AppCompatActivity implements DownloadCallback, SensorEventListener {
    private ProgressDialog mDialog;
    private WebView mWebView;
    private ImageView mImageView;
    private TextView mDataText;
    public static final String TAG_IMG = "IMG";


    private NetworkFragment mNetworkFragment;
    private boolean mDownloading = false;

    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationClient;

    private long ultimaActualizacion = 0;
    private float previoX = 0, previoY = 0, previoZ = 0;
    private float actualX = 0, actualY = 0, actualZ = 0;

    // Servicios SOAP
    private static final String URL = "http://wsgeoip.lavasoft.com/ipservice.asmx";
    private static final String NAMESPACE = "http://wsgeoip.lavasoft.com/";
    private static final String METHOD_NAME = "GetIpLocation";
    private static final String SOAP_ACTION = "http://lavasoft.com/GetIpLocation";
    //Servicios REST
    private static final String URL_REST = "https://restcountries.eu/rest/v2/alpha/CRI";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mWebView = (WebView) findViewById(R.id.webView);
        mImageView = (ImageView) findViewById(R.id.imageView);
        mDataText = (TextView) findViewById(R.id.text);
        invisAll();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    String lastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                    updateUI(location.getLatitude(), location.getLongitude(), lastUpdateTime);
                }
            }
        };
    }

    private void invisAll() {
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
    }
    // Menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void ejemploBarraProgreso() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Ejecutando tarea asincrónica...");
        mDialog.setTitle("Progreso");
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);
        mDialog.setMax(100);
        mDialog.show();
        new TaskBarraProgreso().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.example_one:
                ejemploBarraProgreso();
                return true;
            case R.id.example_two:
                ejemploVistaWeb();
                return true;
            case R.id.example_three:
                ejemploMostrarImagen();
                return true;
            case R.id.example_four_init_action:
                startFragmentTask("https://www.ucr.ac.cr");
                return true;
            case R.id.example_four_fetch_action:
                startDownload();
                return true;
            case R.id.example_four_clear_action:
                finishDownloading();
                mDataText.setText("Cierre de tarea.");
                return true;
            case R.id.example_five_init_action:
                return true;
            case R.id.example_five_fetch_action:
                return true;
            case R.id.example_five_clear_action:
                return true;
            case R.id.example_six:
                inicioServicio();
                return true;
            case R.id.example_seven_init_action:
                startLocationUpdates();
                return true;
            case R.id.example_seven_stop_action:
                stopLocationUpdates();
                return true;
            case R.id.example_eight_connect_action:
                conectarServicioAcelerometro();
                return true;
            case R.id.example_eight_disconnect_action:
                desconectarServicioAcelerometro();
            case R.id.example_nine_init_action:
                servicioSOAP();
                return true;
            case R.id.example_ten_init_action:
                servicioREST();
                return true;
        }
        return false;
    }

    private void servicioREST() {
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
        mDataText.setText("/*/ Llamada a web service REST /*/ \n");
        mDataText.setText(URL_REST);
        new TaskServicioREST().execute(URL_REST);
    }

    private void servicioSOAP() {
        invisAll();
        mDataText.setVisibility(View.VISIBLE);
        mDataText.setText("/*/ Llamada a web service SOAP /*/ \n");
        Thread mJob = new Thread() {
            @Override
            public void run() {
                String resultValue = "\n GetIpLocation: " + "74.125.91.106" + "\n";
// Servicio GetGeoIP
                try {
                    ResultadoGeoIPService mGeoIPService = GetGeoIP("74.125.91.106", NAMESPACE,
                            METHOD_NAME, SOAP_ACTION, URL);
                    resultValue = resultValue + "\n \n" + mGeoIPService.toString();
                } catch (Exception e) {
                    resultValue = resultValue + "\n \n" + "ERROR: " + e.getMessage();
                }
                Message msg = handler.obtainMessage();
                msg.obj = resultValue;
                handler.sendMessage(msg);
            }
        };
        mJob.start();
    }

    public ResultadoGeoIPService GetGeoIP(String mIP, String mNamespace, String mMetodo, String
            mAccion, String mUrl) throws Exception {
        try {
            SoapObject mRequest = new SoapObject(mNamespace, mMetodo);
            PropertyInfo p = new PropertyInfo();
            p.setName("GetIpLocation");
            p.setValue(mIP);
            p.setType(String.class);
            mRequest.addProperty(p);
            PropertyInfo p1 = new PropertyInfo();
            p1.setName("format");
            p1.setValue("soap");
            p1.setType(String.class);
            mRequest.addProperty(p1);
            SoapSerializationEnvelope mEnvelope = new
                    SoapSerializationEnvelope(SoapEnvelope.VER11); //
            mEnvelope.setOutputSoapObject(mRequest);
            mEnvelope.dotNet = true;
            HttpTransportSE mTransporte = new HttpTransportSE(mUrl);
            mTransporte.debug = true;
            mTransporte.call(mAccion, mEnvelope);
            SoapObject resultado = (SoapObject) mEnvelope.bodyIn;
            ResultadoGeoIPService mResultadoGeoIPService = new ResultadoGeoIPService();
            mResultadoGeoIPService.setNombrePais(resultado.getProperty("GetIpLocationResult").toString());
            return mResultadoGeoIPService;
        }catch (Exception e){
            throw(e);
        }
    }

//

    private void conectarServicioAcelerometro() {
        invisAll();
        mDataText.setVisibility(View.VISIBLE);
        mDataText.setText("");
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        List<Sensor> sensors = sm.getSensorList(Sensor.TYPE_ACCELEROMETER);
        if (sensors.size() > 0) {
            sm.registerListener(this, sensors.get(0), SensorManager.SENSOR_DELAY_UI);
        }
    }

    private void desconectarServicioAcelerometro() {
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sm.unregisterListener(this);
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        synchronized (this) {
            long mTimeStamp = event.timestamp;
            actualX = event.values[0];
            actualY = event.values[1];
            actualZ = event.values[2];
            if (previoX == 0 && previoY == 0 && previoZ == 0) {
                ultimaActualizacion = mTimeStamp;
                previoX = actualX;
                previoY = actualY;
                previoZ = actualZ;
            }
            long time_difference = mTimeStamp - ultimaActualizacion;
            if (time_difference > 1000000000L) {
                float movementX = Math.abs(actualX - previoX);
                float movementY = Math.abs(actualY - previoY);
                float movementZ = Math.abs(actualZ - previoX);
                float min_movement = 1.5f;
                if (movementX > min_movement) {
                    String txt = mDataText.getText().toString();
                    txt += "\n /*/Hay Movimiento X/*/ \n\n";
                    mDataText.setText(txt);
                }
                if (movementY > min_movement) {
                    String txt = mDataText.getText().toString();
                    txt += "\n /*/Hay Movimiento Y/*/ \n\n";
                    mDataText.setText(txt);
                }
                if (movementZ > min_movement*3) {
                    String txt = mDataText.getText().toString();
                    txt += "\n /*/Hay Movimiento Z/*/ \n\n";
                    mDataText.setText(txt);
                }
                previoX = actualX;
                previoY = actualY;
                previoZ = actualZ;
                ultimaActualizacion = mTimeStamp;
                String txt = mDataText.getText().toString();
                txt += " /*/Servicio de Acelerómetro/*/ \n" +
                        "X: " + actualX + "\n" +
                        "Y: " + actualY + "\n" +
                        "Z: " + actualZ + "\n";
                mDataText.setText(txt);
            }
        }
    }
    @Override public void onAccuracyChanged(Sensor sensor, int accuracy) { }


    private void updateUI(double latitude, double longitude, String lastUpdateTime) {
        String txt = mDataText.getText().toString();
        txt += " /*/Servicio de Localización/*/ \n" + "Lat: " + String.valueOf(latitude) + "\n" +
                "Lon: " + String.valueOf(longitude) + "\n" + "Time: " + lastUpdateTime + "\n\n";
        mDataText.setText(txt);
    }
    private void startLocationUpdates() {
        Toast.makeText(this, "Iniciar servicio de localización", Toast.LENGTH_LONG).show();
        mDataText.setText("");
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(2500);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        revisarPermiso(Manifest.permission.ACCESS_FINE_LOCATION);
        revisarPermiso(Manifest.permission.ACCESS_COARSE_LOCATION);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "No se cuenta con permisos de localización",
                Toast.LENGTH_LONG).show();
            return;
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback,
                Looper.getMainLooper());
    }
    private void stopLocationUpdates() {
        Toast.makeText(this, "Terminar servicio de localización", Toast.LENGTH_LONG).show();
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }


    private void inicioServicio() {
        invisAll();
        mDataText.setVisibility(View.VISIBLE);
        mDataText.setText("Revisar notificaciones");
        Intent serviceIntent = new Intent(this, ServicioSimple.class);
        serviceIntent.addCategory(ServicioSimple.SERVICE_TAG);
        startService(serviceIntent);
    }

    private void startFragmentTask(String url) {
        invisAll();
        mDataText.setVisibility(View.VISIBLE);
        mDataText.setText(url);
        mNetworkFragment = NetworkFragment.getInstance(getSupportFragmentManager(), url);
    }
    private void startDownload() {
        if (!mDownloading && mNetworkFragment != null) {
            mNetworkFragment.startDownload();
            mDownloading = true;
        }
    }
    @Override
    public void updateFromDownload(String result) {
        if (result != null) {
            mDataText.setText(result);
            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        } else {
            mDataText.setText(result);
            Toast.makeText(getApplicationContext(), "Error de Conexion",
                    Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        switch (progressCode) {
            case Progress.ERROR:
                Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
                break;
            case Progress.CONNECT_SUCCESS:
                Toast.makeText(getApplicationContext(), "CONNECT_SUCCESS",
                        Toast.LENGTH_SHORT).show();
                break;
            case Progress.GET_INPUT_STREAM_SUCCESS:
                Toast.makeText(getApplicationContext(), "GET_INPUT_STREAM_SUCCESS",
                        Toast.LENGTH_SHORT).show();
                break;
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:
                Toast.makeText(getApplicationContext(), "PROCESS_INPUT_STREAM_IN_PROGRESS",
                        Toast.LENGTH_SHORT).show();
                break;
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                Toast.makeText(getApplicationContext(), "PROCESS_INPUT_STREAM_SUCCESS",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }
    @Override public NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo;
    }
    @Override
    public void finishDownloading() {
        mDownloading = false;
        if (mNetworkFragment != null) {
            mNetworkFragment.cancelDownload();
        }
        NetworkFragment networkFragment = (NetworkFragment) getSupportFragmentManager()
                .findFragmentByTag(NetworkFragment.TAG_FRAGMENT);
        if (networkFragment != null) {
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().remove(networkFragment).commit();
            manager.popBackStack();
        }
    }


    private void ejemploMostrarImagen() {
        if( revisarPermiso(Manifest.permission.INTERNET) ) {
            invisAll();
            mImageView.setVisibility(View.VISIBLE);
            new TaskMostrarImagen().execute("https://medios.ucr.ac.cr/medios/fotos/pri_x-large/2020/laguna-botos-volcan-poas5f63d0f9ea732.jpg");
        } else {
            Toast.makeText(this, "No se cuenta con permiso de Internet", Toast.LENGTH_LONG);
        }
    }

    protected boolean revisarPermiso(String permissionType) {
        if (ContextCompat.checkSelfPermission(this, permissionType) !=
                PackageManager.PERMISSION_GRANTED )
            solicitarPermiso(permissionType, PackageManager.PERMISSION_GRANTED);
        return ContextCompat.checkSelfPermission(this, permissionType) ==
                PackageManager.PERMISSION_GRANTED;
    }
    protected void solicitarPermiso(String permissionType, int requestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permissionType}, requestCode);
    }
    private void ejemploVistaWeb() {
        if( revisarPermiso(Manifest.permission.INTERNET) ) {
            invisAll();
            mWebView.setVisibility(View.VISIBLE);
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setLoadsImagesAutomatically(true);
            mWebView.loadUrl("https://www.ecci.ucr.ac.cr");
        } else {
            Toast.makeText(this, "No se cuenta con permiso de Internet", Toast.LENGTH_LONG);
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String txt = (String) msg.obj;
            mDataText.append("\n" + txt);
        }
    };

    private class TaskServicioREST extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return loadContentFromNetwork(urls[0]);
        }
        protected void onPostExecute(String result) {
            mDataText.append("\n \n" + result);
        }
        private String loadContentFromNetwork(String url) {
            try {
                InputStream mInputStream = (InputStream) new URL(url).getContent();
                InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
                BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);
                StringBuilder strBuilder = new StringBuilder();
                String line = null;
                while ((line = responseBuffer.readLine()) != null) {
                    strBuilder.append(line);
                }
                return strBuilder.toString();
            } catch (Exception e) {
                Log.v(TAG_IMG, e.getMessage());
            }
            return null;
        }
    }
                //

    private class TaskMostrarImagen extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            return loadImageFromNetwork(urls[0]);
        }
        protected void onPostExecute(Bitmap result) {
            mImageView.setImageBitmap(result);
        }
        private Bitmap loadImageFromNetwork(String url) {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(
                        (InputStream) new URL(url).openStream());
                return bitmap;
            } catch (Exception e) {
                Log.v(TAG_IMG, e.getMessage());
            }
            return null;
        }
    }

    private class TaskBarraProgreso extends AsyncTask<String, Float, Integer> {
        @Override protected void onPreExecute() {
// antes de ejecutar
            mDialog.setProgress(0);
        }
        @Override protected Integer doInBackground(String... params) {
// Cuando se dispara la tarea simulamos el procesamiento en la tarea asincrnica
            for (int i = 0; i < 100; i++) {
                try { Thread.sleep(50); }
                catch (InterruptedException e) { }
// publicamos el progreso de la tarea
                publishProgress(i/100f);
            }
            return 100;
        }
        protected void onProgressUpdate (Float... values) {
// actualizamos la barra de progreso y sus valores
            int p = Math.round(100 * values[0]);
            mDialog.setProgress(p);
        }
        protected void onPostExecute(Integer bytes) {
// Cuando la tarea termina cerramos la barra de progreso
            mDialog.dismiss();
        }
    }
}