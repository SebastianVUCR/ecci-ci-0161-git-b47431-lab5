package cr.ac.ucr.ecci.eseg.miserviciossensores;
        import android.app.NotificationChannel;
        import android.app.NotificationManager;
        import android.app.Service;
        import android.content.Intent;
        import android.os.Build;
        import android.os.Handler;
        import android.os.HandlerThread;
        import android.os.IBinder;
        import android.os.Looper;
        import android.os.Process;
        import android.os.Message;
        import android.widget.Toast;
        import androidx.core.app.NotificationCompat;
        import androidx.core.app.NotificationManagerCompat;

public class ServicioSimple extends Service {
    public static final String SERVICE_TAG =
            "cr.ac.ucr.ecci.eseg.mirestsoap.ServicioSimple.ServiceTag";
    public static final String SERVICE_RESULT =
            "cr.ac.ucr.ecci.eseg.mirestsoap.ServicioSimple.REQUEST_PROCESSED";
    public static final String SERVICE_MESSAGE =
            "cr.ac.ucr.ecci.eseg.mirestsoap.ServicioSimple.COPA_MSG";
    public static final String CHANNEL_ID = "ESEG-ServicioSimple";
    private Looper serviceLooper;
    private ServiceHandler serviceHandler;
    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            try {
                enviarResultado("Este es un mensaje desde el servicio número 0!", 0);
                Thread.sleep(5000);
                enviarResultado("Este es un mensaje desde el servicio número 1!", 1);
                Thread.sleep(5000);
                enviarResultado("Este es un mensaje desde el servicio número 2!", 2);
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            stopSelf(msg.arg1);
        }
        private void enviarResultado(String msg, int notificationId) {
            NotificationCompat.Builder builder = new
                    NotificationCompat.Builder(getBaseContext(), CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle("Servicio Simple")
                    .setContentText(msg)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(getBaseContext());
            notificationManager.notify(notificationId, builder.build());
        }
    }
    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        createNotificationChannel();
        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);
    }
    private void createNotificationChannel() {
// Create the NotificationChannel, but only on API 26+ because
// the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "ESEG Servicio Simple";
            String description = "Prueba";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name,importance);
            channel.setDescription(description);
// Register the channel with the system; you can't change the importance
// or other notification behaviors after this
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
    @Override public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Inicio de servicios", Toast.LENGTH_SHORT).show();
        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = startId;
        serviceHandler.sendMessage(msg);
        return START_NOT_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override public void onDestroy () {
        super.onDestroy();
        Toast.makeText(this, "Fin de servicio", Toast.LENGTH_SHORT).show();
    }
}